﻿#pragma once

#define WIN64_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers

#include <iostream>
#include <stdio.h>

namespace msl {

	void endline();

	class OutStream
	{
	protected:
		FILE* myFile;
	public:
		OutStream();
		~OutStream();

		OutStream& operator<<(const char* str);
		OutStream& operator<<(int num);
		OutStream& operator<<(void(*pf)());

	};
}
