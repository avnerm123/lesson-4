#pragma once

#include "OutStream.h"

#define MIN_ASCCI 32
#define MAX_ASCCI 126
#define CONVERT MAX_ASCCI - MIN_ASCCI



class OutStreamEncrypted : public OutStream {
	int _haste;
public:
	OutStreamEncrypted(int haste);

	OutStreamEncrypted& OutStreamEncrypted::operator<<(std::string str);

	OutStreamEncrypted& operator<<(int num);

};