#pragma once
#include <iostream>
#include <stdio.h>

void endline();

class OutStream
{
protected:
	FILE* myFile;
public:
	OutStream();
	~OutStream();

	OutStream& operator<<(const char *str);
	OutStream& operator<<(int num);
	OutStream& operator<<(void(*pf)());
	
};
