#pragma once

#define _CRT_SECURE_NO_DEPRECATE
#include "FileStream.h"
#include <stdio.h>
#include <iostream>



FileStream::FileStream(const char* path)
{
	FILE* file = fopen(path, "w+");
	this->myFile = file;
}

FileStream::~FileStream()
{
	fclose(this->myFile);
	delete this->myFile;
}

