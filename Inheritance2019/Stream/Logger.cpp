#include "Logger.h"

Logger::Logger(const char* filename, bool logToScreen)
{
	this->myFile = new FileStream(filename);
	this->myCout = new OutStream;
	this->_logToScreen = logToScreen;
	this->num_of_line = 0;
}

Logger::~Logger()
{
	
	delete this->myCout;
	delete this->myFile;
}

void Logger::print(const char* msg)
{
	if (this->_logToScreen)
	{
		*(this->myCout) << "The number of line is:"<<this->num_of_line << endline <<msg << endline;
	}
	*(this->myFile) << "The number of line is:" << this->num_of_line << endline << msg << endline;
	this->num_of_line++;
}
