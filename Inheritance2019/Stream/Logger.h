#pragma once
#include "FileStream.h"
#include "OutStream.h"

class Logger
{
	FileStream* myFile;
	OutStream* myCout;
	bool _logToScreen;
	unsigned int num_of_line;
public:
	Logger(const char *filename, bool logToScreen);
	~Logger();

	void print(const char *msg);
};
