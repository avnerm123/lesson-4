#include "OutStreamEncrypted.h"
#include <string>




OutStreamEncrypted::OutStreamEncrypted(int haste) : OutStream::OutStream(), _haste(haste)
{
}

OutStreamEncrypted& OutStreamEncrypted::operator<<(std::string str)
{
	for (int i = 0; i < str.size(); i++)
	{
		if (int(str[i]) >= MIN_ASCCI && int(str[i]) <= MAX_ASCCI)
		{
			if (str[i] == ' ')
			{

			}
			else if (int(str[i]) + this->_haste > MAX_ASCCI)
			{
				str[i] = (int(str[i]) + this->_haste) - CONVERT;
			}
			else {
				str[i] = str[i] + this->_haste;
			}
		}
	}

	printf("%s", str.c_str());
	return *this;
}


OutStreamEncrypted& OutStreamEncrypted::operator<<(int num)
{
	char c_num = num + '0';
	c_num += this->_haste;
	printf("%c", c_num);
	return *this;
}

