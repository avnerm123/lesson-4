#include "OutStream.h"
#include <stdio.h>

FILE* file = stdout;

void endline()
{
	fprintf(file, "\n");
}


OutStream::OutStream()
{
	this->myFile = stdout
;
}

OutStream::~OutStream()
{
}

OutStream& OutStream::operator<<(const char *str)
{
	fprintf(this->myFile, "%s", str);

	return *this;
}

OutStream& OutStream::operator<<(int num)
{
	fprintf(this->myFile, "%d", num);
	return *this;
}

OutStream& OutStream::operator<<(void(*pf)())
{
	file = this->myFile;
	pf();
	return *this;
}

