#pragma once
#include <iostream>
#include "OutStream.h"
#include <string>

class FileStream : public OutStream
{
public:
	FileStream(const char* path);
	~FileStream();
	
};

void endline();